Source: singularity-container
Section: admin
Priority: optional
Standards-Version: 4.4.1
Maintainer: Debian HPC Team <debian-hpc@lists.debian.org>
Uploaders: Dave Love <fx@gnu.org>,
           Mehdi Dogguy <mehdi@debian.org>,
           Yaroslav Halchenko <debian@onerussian.com>,
           Afif Elghraoui <afif@debian.org>,
           Dmitry Smirnov <onlyjob@debian.org>,
Build-Depends: debhelper (>= 12~) ,dh-golang
    ,cryptsetup-bin
    ,golang-any
    ,golang-github-apex-log-dev
    ,golang-github-appc-cni-dev (>= 0.7.1~)
      ,golang-github-containernetworking-plugins-dev
    ,golang-github-blang-semver-dev
    ,golang-github-buger-jsonparser-dev
    ,golang-github-containerd-cgroups-dev
#   ,golang-github-containerd-containerd-dev (>= 1.2.6~) :: (provided by docker)
    ,golang-github-containers-image-dev
#        ,golang-github-coreos-bbolt-dev
#        ,golang-github-docker-distribution-dev
#        ,golang-github-docker-go-connections-dev
#        ,golang-github-ulikunitz-xz-dev
#        ,golang-github-vbauerster-mpb-dev
    ,golang-github-containers-storage-dev
    ,golang-github-docker-docker-credential-helpers-dev
    ,golang-github-docker-docker-dev (>= 19.03.4~)
    ,golang-github-docker-libtrust-dev
    ,golang-github-gorilla-websocket-dev
    ,golang-github-hashicorp-go-retryablehttp-dev (>= 0.6.2~)
    ,golang-github-j-keck-arping-dev
    ,golang-github-opencontainers-image-spec-dev
    ,golang-github-opencontainers-runtime-tools-dev
    ,golang-github-opencontainers-selinux-dev
    ,golang-github-opencontainers-specs-dev
    ,golang-github-opensuse-umoci-dev
    ,golang-github-pelletier-go-toml-dev
    ,golang-github-pkg-errors-dev
    ,golang-github-pquerna-ffjson-dev
    ,golang-github-safchain-ethtool-dev
    ,golang-github-satori-go.uuid-dev
    ,golang-github-spf13-cobra-dev
    ,golang-github-spf13-pflag-dev
    ,golang-github-sylabs-json-resp-dev
#   ,golang-golang-x-crypto-dev :: (patched by upstream)
    ,golang-golang-x-sys-dev
    ,golang-gopkg-cheggaaa-pb.v1-dev
    ,golint <!nocheck>
    ,help2man
    ,libgpgme-dev
    ,libssl-dev
    ,stow
    ,uuid-dev
Homepage: http://www.sylabs.io
Vcs-Git: https://salsa.debian.org/hpc-team/singularity-container.git
Vcs-Browser: https://salsa.debian.org/hpc-team/singularity-container
XS-Go-Import-Path: github.com/sylabs/singularity

Package: singularity-container
Architecture: any
Built-Using: ${misc:Built-Using}
Depends: ${misc:Depends} ,${shlibs:Depends}
    ,ca-certificates
    ,containernetworking-plugins
    ,squashfs-tools
Recommends: e2fsprogs,
Description: container platform focused on supporting "Mobility of Compute"
 Mobility of Compute encapsulates the development to compute model
 where developers can work in an environment of their choosing and
 creation and when the developer needs additional compute resources,
 this environment can easily be copied and executed on other platforms.
 Additionally as the primary use case for Singularity is targeted
 towards computational portability, many of the barriers to entry of
 other container solutions do not apply to Singularity making it an
 ideal solution for users (both computational and non-computational)
 and HPC centers.

Package: golang-github-sylabs-singularity-dev
Section: devel
Architecture: all
Depends: ${misc:Depends}
    ,golang-github-appc-cni-dev (>= 0.7.1~)
    ,golang-github-blang-semver-dev
    ,golang-github-containernetworking-plugins-dev
    ,golang-github-containers-image-dev
    ,golang-github-gorilla-websocket-dev
    ,golang-github-opencontainers-image-spec-dev
    ,golang-github-opencontainers-selinux-dev
    ,golang-github-opencontainers-specs-dev
    ,golang-github-opensuse-umoci-dev
    ,golang-github-pelletier-go-toml-dev
    ,golang-github-satori-go.uuid-dev
    ,golang-github-spf13-cobra-dev
    ,golang-github-spf13-pflag-dev
    ,golang-github-sylabs-json-resp-dev
    ,golang-golang-x-crypto-dev
    ,golang-gopkg-cheggaaa-pb.v1-dev
    ,golang-gopkg-yaml.v2-dev
Description: development files for Singularity application container engine
 Mobility of Compute encapsulates the development to compute model
 where developers can work in an environment of their choosing and
 creation and when the developer needs additional compute resources,
 this environment can easily be copied and executed on other platforms.
 Additionally as the primary use case for Singularity is targeted
 towards computational portability, many of the barriers to entry of
 other container solutions do not apply to Singularity making it an
 ideal solution for users (both computational and non-computational)
 and HPC centers.
 .
 This package provides Golang sources for the Singularity API.
